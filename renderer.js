// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.

require('materialize-css');
require('material-design-icons');
const openFilesManager = require('./module/openFilesManager.js');
const displayerManager = require('./module/displayerManager.js');

/*

Ceci est un exemple de comment utiliser les manager de fichier.
Vous devez appeler le constructeur "openFilesManager" avec l'element
auquel vous voulez le relier


var element = document.getElementById("id");
const file = new openFilesManager(element, null);
*/

window.onload = () => {
    const manager = new displayerManager(document.getElementById("app"));

    var elems = document.querySelectorAll('select');
    console.log(elems)
    var instances = M.FormSelect.init(elems, {});
    const element = document.getElementById("id");

    element.onclick = () => {
        manager.read();
    };
};
