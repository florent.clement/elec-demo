const anime = require('animejs');


module.exports = function (animationName, elementToAnimate) {
    let animationsList = {
        'square' : {
            targets: 'none',
            translateX: [
                { value: 250, duration: 1000, delay: 500 },
                { value: 0, duration: 1000, delay: 500 }
            ],
            translateY: [
                { value: -40, duration: 500 },
                { value: 40, duration: 500, delay: 1000 },
                { value: 0, duration: 500, delay: 1000 }
            ],
            scaleX: [
                { value: 4, duration: 100, delay: 500, easing: 'easeOutExpo' },
                { value: 1, duration: 900 },
                { value: 4, duration: 100, delay: 500, easing: 'easeOutExpo' },
                { value: 1, duration: 900 }
            ],
            scaleY: [
                { value: [1.75, 1], duration: 500 },
                { value: 2, duration: 50, delay: 1000, easing: 'easeOutExpo' },
                { value: 1, duration: 450 },
                { value: 1.75, duration: 50, delay: 1000, easing: 'easeOutExpo' },
                { value: 1, duration: 450 }
            ],
            easing: 'easeOutElastic(1, .8)',
            loop: false
        },
        'grid' : {
            targets: '.staggering-grid-demo .el',
            scale: [
                {value: .1, easing: 'easeOutSine', duration: 500},
                {value: 1, easing: 'easeInOutQuad', duration: 1200}
            ],
            delay: anime.stagger(200, {grid: [1, 1], from: 'center'})
        },
        'from' : {
            targets: '.el.from-to-values',
            translateX: [100, 250], // from 100 to 250
            delay: 500,
            direction: 'alternate',
            loop: false
        },
        'relative' : {
            targets: '.el.relative-values',
            translateX: {
                value: '*=2.5', // 100px * 2.5 = '250px'
                duration: 1000
            },
            width: {
                value: '-=20px', // 28 - 20 = '8px'
                duration: 1800,
                easing: 'easeInOutSine'
            },
            rotate: {
                value: '+=2turn', // 0 + 2 = '2turn'
                duration: 1800,
                easing: 'easeInOutSine'
            },
            direction: 'alternate'
        },
        'svg' : {
            targets: '.css-prop-demo .el',
            left: '240px',
            backgroundColor: '#FFF',
            borderRadius: ['0%', '50%'],
            easing: 'easeInOutQuad'
        }
    }
    let selectedAnimation = animationsList[animationName];
    if (selectedAnimation){
        selectedAnimation['targets'] = elementToAnimate;
        anime(selectedAnimation)
    }
}
