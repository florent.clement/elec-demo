const {remote, app } = require('electron'),
    dialog = remote.dialog;
const fs = require('fs');
const path = require('path');
const assert = require('assert').strict;

/*
 Documentation : https://www.brainbell.com/javascript/show-open-dialog.html

 Upload de fichier: https://stackoverflow.com/questions/53979681/image-file-upload-and-retrieve-in-electron


 */


/**
 *  L'openFileManager à pour but d'ajouter les fonctions et events
 *  nécessaire afin d'ouvrir une liste multiple de fichier
 *  Une fonction callback pour être utilisé afin de gérer l'utilisateur de ces fichiers
 *
 *
 *
 * @param element Element du DOM ( de préférence un bouton )
 * @param callback Fonction en callback qui est appellé quand une liste de fichier est séléectionné par l'utilisateur
 */
module.exports = function (element, callback) {

  /**
   * Liste des fichiers enregistrés dans dist
   * @type {*[]}
   */
  this.store = [];

  /**
   * Répertoire de sauvegarde des fichiers
   * @type {string}
   */
  const savePath = "dist";


  /**
   * Créer un répertoire data dans public pour pouvoire sauvegarder des images dedans
   * Si le fichier existe déjà on supprime tout son contenu
   */
  this.createDirectory = function () {

    fs.mkdir(savePath, (err) => {
      if (err) {
        fs.readdir(savePath, (err, files) => {
          if (err) throw err;

          for (const file of files) {
            fs.unlink(path.join(savePath, file), err => {
              if (err) throw err;
            });
          }
        });
      }
    });
  };

  /**
   * Sauvegarde des fichiers dans le répertoire
   * @param pathFile Path local du client
   */
  this.saveFile = function(pathFile, countFiles) {
    const fileName = path.basename(pathFile);
    fs.copyFile(pathFile, savePath + '/' + fileName, (err) => {
      if (err) throw err;
      this.store.push(savePath + '/' +  fileName);

      if( callback != null && this.store.length === countFiles) {
        callback(this.store);
      }
    });
  };





  assert.notEqual(element, null, "Le bouton n'existe pas");

  this.createDirectory();

  element.onclick = () => {

    let options = {

      filters :[
        {name: 'Images', extensions: ['jpg', 'png', 'gif']},
        {name: 'PDF', extensions: ['pdf']},
        {name: 'All Files', extensions: ['*']}
      ],
      properties: ['openFile','multiSelections']
    };

    this.createDirectory();


    dialog.showOpenDialog(null, options)
        .then((openDialogReturnValue) => {

          this.store = [];
          const listFiles = openDialogReturnValue.filePaths;

          for (let i in listFiles) {
            this.saveFile(listFiles[i], listFiles.length);
          }
    });

  }

};