const $ = require('jquery');
require('hammerjs');
const DropZone = require('dropzone');

module.exports = function (elementId) {

  const element = document.getElementById(elementId);

  const previewNode = document.querySelector("#zdrop-template");
  previewNode.id = "";
  const previewTemplate = previewNode.parentNode.innerHTML;
  previewNode.parentNode.removeChild(previewNode);

  this.zdrop = new DropZone("#" + elementId, {
    url: '/Home/UploadFile',
    maxFilesize: 50,
    previewTemplate: previewTemplate,
    acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
    autoQueue: false,
    previewsContainer: "#previews",
    clickable: "#" + elementId
  });

  this.zdrop.on("totaluploadprogress", function (progress) {
    var progr = document.querySelector(".progress .determinate");
    if (progr === undefined || progr === null)
      return;

    progr.style.width = progress + "%";
  });

  this.zdrop.on('dragenter', function () {
    // $('#zdrop').css({ background: '#41ab6b', color: '#fff' });
    $(element).addClass('green lighten-1 white-text');
  });

  this.zdrop.on('dragleave', function () {
    // $('#zdrop').css({ background: '#fff', color: '#000' });
    $(element).addClass('white black-text').removeClass('green lighten-1 white-text');
  });

  this.zdrop.on('drop', function () {
    //  $('#zdrop').css({ background: '#fff', color: '#333' });
    $(element).addClass('white black-text').removeClass('green lighten-1 white-text');
  });

  this.getListFiles = () => {
    return this.zdrop.getAddedFiles().map(function(file) {
      return file.path;
    }).filter((value, index, self) => {
      return self.indexOf(value) === index;
    });
  }
};