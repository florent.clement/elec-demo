const assert = require('assert').strict;
const MouseTrap = require('mousetrap');

const DropZoneManager = require("./dropZoneManager.js");
const AnimationManager = require("./AnimationManager.js");


/**
 * Outil pour afficher une liste d'image
 * @param element
 */
module.exports = function (element) {

  const self = this;
  /*
    Position de l'image à afficher
   */
  this.current = 0;

  /*
    Liste des images
   */
  this.listPictures = [];


  this.createNodeImg = function(path) {
    const div = document.createElement("div");
    const img = document.createElement("img");
    img.src = path;
    div.append(img);
    div.style.display = "none";
    return div;
  };

  this.replaceNodeImg = function(pos, path) {
    this.divList.childNodes[pos].childNodes[0].src = path;
  };

  /**
   * List d'image
   * @param listPictures
   */
  this.update = function (listPictures) {

    for (let i in listPictures) {
      if (i + 1 > this.listPictures.length ) {
        this.divList.append(this.createNodeImg(listPictures[i]));

      } else {
        if (this.listPictures[i] !== listPictures[i] ) {
          this.replaceNodeImg(i, listPictures[i]);
        }
      }
    }

    while(this.divList.childNodes.length > listPictures.length) {
      this.divList.removeChild(this.divList.lastChild);
    }

    this.current = 0;

    this.listPictures = listPictures;


    this.dispay(this.current);
  };

  this.read = function () {
    const listFiles = this.dropZoneManager.getListFiles();
    if (listFiles) {
      this.update(this.dropZoneManager.getListFiles());
    }

    if(listFiles.length > 0 ) {
      document.getElementById("zdrop").parentNode.style.display = "none";
      document.getElementById("id").style.display = "none";
      self.divList.style.display = "block";
      self.closeButton.style.visibility = "visible";
    }

  };

  this.dispay = function(index) {

    if (index >= 0 && index < this.listPictures.length) {
      this.divList.childNodes[this.current].style.display = "none";
      this.divList.childNodes[index].style.display = "block";
      AnimationManager(document.getElementById("animation-selector").value, this.divList.childNodes[index])
      this.current = index;
    } else if(this.current > this.listPictures.length) {
      this.current = this.listPictures - 1;
      this.divList.childNodes[this.current].style.display = "none";
    }

    this.nextButton.style.visibility = this.current + 1 < this.listPictures.length  ? "visible" : "hidden";
    this.previousButton.style.visibility = this.current === 0  ? "hidden" : "visible";


  };

  this.next = function () {
    if (this.current < this.listPictures.length){
      this.dispay(this.current + 1);
    }
  };

  this.previous = function () {
    if (this.current > 0) {
      this.dispay(this.current - 1);
    }
  };

  this.close = function () {
    document.getElementById("zdrop").parentNode.style.display = "block";
    document.getElementById("id").style.display = "block";
    self.divList.style.display = "none";
    self.closeButton.style.visibility = "hidden";
    self.update([]);
  };

  this.initButton = function () {
    // next button
    this.nextButton = document.createElement("button");
    this.nextButton.classList.add("next");
    this.nextButton.onclick = () => {
      this.next();
    };
    this.nextButton.style.visibility = "hidden";

    // previous button
    this.previousButton = document.createElement("button");
    this.previousButton.classList.add("prev");
    this.previousButton.onclick = () => {
      this.previous();
    };
    this.previousButton.style.visibility = "hidden";

    // close button
    this.closeButton = document.createElement("button");
    this.closeButton.classList.add("close");
    this.closeButton.onclick = () => {
      this.close();
    };
    this.closeButton.textContent = "X";
    this.closeButton.style.visibility = "hidden";


    element.append(this.previousButton);
    element.append(this.nextButton);
    element.append(this.closeButton);
    element.append(this.closeButton);
  };

  assert.notEqual(element, null, "La div n'éxiste pas");

  element.innerHTML = "";
  this.divList = document.createElement("div");
  this.divList.style.height = '100vh';
  this.divList.style.display = "none";
  element.append(this.divList);

  // initialisation contrôle
  this.initButton();
  MouseTrap.bind("left", () => {
    this.previous();
  });
  MouseTrap.bind("right", () => {
    this.next();
  });

  MouseTrap.bind("esc", () => {
    self.close();
  });

  this.dropZoneManager = new DropZoneManager("zdrop");
};
